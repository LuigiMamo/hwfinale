DROP DATABASE IF EXISTS hwfinale;
CREATE DATABASE  hwfinale;
USE hwfinale;
CREATE TABLE cliente(
	cliente_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome_azienda VARCHAR(50) NOT NULL,
    indirizzo VARCHAR (250)
);


CREATE TABLE preventivo(
	preventivo_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    note TEXT,
    emittente VARCHAR (50) NOT NULL,
    indirizzo_emittente VARCHAR (250) NOT NULL,
    quantita INTEGER NOT NULL DEFAULT(1),
    totale INTEGER NOT NULL,
    cliente_rif INTEGER,
    FOREIGN KEY(cliente_rif) REFERENCES cliente(cliente_id) ON DELETE CASCADE
);


CREATE TABLE prodotto(
	prodotto_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    codice VARCHAR(25),
    nome VARCHAR (50),
    prezzo INTEGER NOT NULL DEFAULT(1)
);

CREATE TABLE categoria(
	categoria_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR (50)
);

CREATE TABLE preventivo_prodotto(
	preventivo_rif INTEGER NOT NULL,
    prodotto_rif INTEGER NOT NULL,
    FOREIGN KEY(preventivo_rif) REFERENCES preventivo(preventivo_id) ON DELETE CASCADE,
    FOREIGN KEY(prodotto_rif) REFERENCES prodotto(prodotto_id) ON DELETE CASCADE
);

CREATE TABLE prodotto_categoria(
	prodotto_rif INTEGER NOT NULL,
    categoria_rif INTEGER NOT NULL,
    FOREIGN KEY(prodotto_rif) REFERENCES prodotto(prodotto_id)ON DELETE CASCADE,
    FOREIGN KEY(categoria_rif) REFERENCES categoria(categoria_id) ON DELETE CASCADE
);

SELECT * FROM prodotto;
SELECT * FROM preventivo_prodotto;
SELECT * FROM cliente;
SELECT * FROM preventivo;