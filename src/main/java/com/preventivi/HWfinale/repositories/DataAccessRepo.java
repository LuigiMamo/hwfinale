package com.preventivi.HWfinale.repositories;

import java.util.List;

import org.springframework.stereotype.Repository;
@Repository
public interface DataAccessRepo<T> {
	
	
	T insert (T t);
	T findById(int t);
	List<T> findAll();
	boolean delete(int t);
	boolean update(T t);
	

}
