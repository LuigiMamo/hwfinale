package com.preventivi.HWfinale.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.preventivi.HWfinale.models.Cliente;
import com.preventivi.HWfinale.repositories.DataAccessRepo;

@Service
public class ClienteService implements DataAccessRepo<Cliente>{
	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}

	@Override
	public Cliente insert(Cliente t) {
		Cliente temp = new Cliente();
		temp.setNome_azienda(t.getNome_azienda());
		temp.setIndirizzo(t.getIndirizzo());
		
		Session sessione = getSessione();
		try {
			sessione.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		return temp;
	}	

	@Override
	public Cliente findById(int t) {
		Session sessione = getSessione();
		return (Cliente) sessione
				.createCriteria(Cliente.class)
				.add(Restrictions.eqOrIsNull("cliente_id", t))
				.uniqueResult();
	}

	@Override
	public List<Cliente> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Cliente.class).list();
	}

	@Override
	@Transactional
	public boolean delete(int t) {
		
		Session sessione = getSessione();
		try {
			Cliente temp = sessione.load(Cliente.class, t);
			
			sessione.delete(temp);
			sessione.flush();
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}

	@Override
	@Transactional
	public boolean update(Cliente t) {
		Session sessione = getSessione();
		try {
			Cliente temp = sessione.load(Cliente.class, t.getCliente_id());
			
			if(temp!=null) {
				temp.setNome_azienda(t.getNome_azienda());
				temp.setIndirizzo(t.getIndirizzo());
				
				sessione.update(t);
				sessione.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	

}
