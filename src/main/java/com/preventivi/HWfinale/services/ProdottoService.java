package com.preventivi.HWfinale.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.preventivi.HWfinale.models.Prodotto;
import com.preventivi.HWfinale.repositories.DataAccessRepo;

@Service
public class ProdottoService implements DataAccessRepo<Prodotto>{

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}
		
	
	@Override
	public Prodotto insert(Prodotto t) {
		Prodotto temp = new Prodotto();
		temp.setNome(t.getNome());
		temp.setCodice(t.getCodice());
		temp.setPrezzo(t.getPrezzo());
		
		Session sessione = getSessione();
		try {
			sessione.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		return temp;
	}

	@Override
	public Prodotto findById(int t) {
		Session sessione = getSessione();
		return (Prodotto) sessione
				.createCriteria(Prodotto.class)
				.add(Restrictions.eqOrIsNull("prodotto_id", t))
				.uniqueResult();
	}

	@Override
	public List<Prodotto> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Prodotto.class).list();
	}

	@Override
	@Transactional
	public boolean delete(int t) {
		Session sessione = getSessione();
		try {
			Prodotto temp = sessione.load(Prodotto.class, t);
			
			sessione.delete(temp);
			sessione.flush();
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}

	@Override
	@Transactional
	public boolean update(Prodotto t) {
		Session sessione = getSessione();
		try {
			Prodotto temp = sessione.load(Prodotto.class, t.getProdotto_id());
			
			if(temp!=null) {
				temp.setNome(t.getNome());
				temp.setCodice(t.getCodice());
				temp.setPrezzo(t.getPrezzo());
				
				sessione.update(t);
				sessione.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

}
