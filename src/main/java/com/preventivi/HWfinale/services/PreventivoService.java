package com.preventivi.HWfinale.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.preventivi.HWfinale.models.Preventivo;
import com.preventivi.HWfinale.repositories.DataAccessRepo;

@Service
public class PreventivoService implements DataAccessRepo<Preventivo>{
	@Autowired
	private EntityManager entMan;

	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}
	
	@Override
	public Preventivo insert(Preventivo t) {
		Preventivo temp = new Preventivo();
		temp.setNote(t.getNote());
		temp.setEmittente(t.getEmittente());
		temp.setIndirizzo_emittente(t.getIndirizzo_emittente());
		temp.setQuantita(t.getQuantita());
		
		Session sessione = getSessione();
		try {
			sessione.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		return temp;
	}

	@Override
	public Preventivo findById(int t) {
		Session sessione = getSessione();
		return (Preventivo) sessione
				.createCriteria(Preventivo.class)
				.add(Restrictions.eqOrIsNull("preventivo_id", t))
				.uniqueResult();
	}

	@Override
	public List<Preventivo> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Preventivo.class).list();
	}

	@Override
	@Transactional
	public boolean delete(int t) {
			
			Session sessione = getSessione();
			try {
				Preventivo temp = sessione.load(Preventivo.class, t);
				
				sessione.delete(temp);
				sessione.flush();
				return true;
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			return false;
	}

	@Override
	@Transactional
	public boolean update(Preventivo t) {
			Session sessione = getSessione();
			try {
				Preventivo temp = sessione.load(Preventivo.class, t.getPreventivo_id());
				
				if(temp!=null) {
					temp.setNote(t.getNote());
					temp.setEmittente(t.getEmittente());
					temp.setIndirizzo_emittente(t.getIndirizzo_emittente());
					temp.setQuantita(t.getQuantita());
					
					sessione.update(t);
					sessione.flush();
					return true;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return false;
	}
	
	
}
