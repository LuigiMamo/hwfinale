package com.preventivi.HWfinale.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.preventivi.HWfinale.models.Cliente;
import com.preventivi.HWfinale.services.ClienteService;

@RestController
@RequestMapping("/cliente")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@PostMapping("/insert")
	public Cliente insert(@RequestBody Cliente varCliente) {
		return service.insert(varCliente);
	}
	
	@GetMapping("/{varId}")
	public Cliente clienteById(@PathVariable int varId) {
		return service.findById(varId);
	}
	
	@GetMapping("/list")
	public List<Cliente> allClienti() {
		return service.findAll();
	}
	
	@DeleteMapping("/{cliente_id}")
	public boolean deleteCliente(@PathVariable int cliente_id) {
		return service.delete(cliente_id);
	}
	
	@PutMapping("/update")
	public boolean updateCliente(@RequestBody Cliente varCliente) {
		return service.update(varCliente);
	}
}
