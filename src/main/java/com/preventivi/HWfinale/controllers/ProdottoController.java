package com.preventivi.HWfinale.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.preventivi.HWfinale.models.Prodotto;
import com.preventivi.HWfinale.services.ProdottoService;


@RestController
@RequestMapping("/prodotto")
@CrossOrigin(origins ="*", allowedHeaders = "*" )
public class ProdottoController {
	
	@Autowired
	private ProdottoService service;
	
	@PostMapping("/insert")
	public Prodotto insert(@RequestBody Prodotto varProdotto) {
		return service.insert(varProdotto);
	}
	
	@GetMapping("/{varId}")
	public Prodotto prodottoById(@PathVariable int varId) {
		return service.findById(varId);
	}
	
	@GetMapping("/list")
	public List<Prodotto> allClienti() {
		return service.findAll();
	}
	
	@DeleteMapping("/{cliente_id}")
	public boolean deleteProdotto(@PathVariable int prodotto_id) {
		return service.delete(prodotto_id);
	}
	
	@PutMapping("/update")
	public boolean updateProdotto(@RequestBody Prodotto varProdotto) {
		return service.update(varProdotto);
	}
	

}
