package com.preventivi.HWfinale.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.preventivi.HWfinale.models.Preventivo;
import com.preventivi.HWfinale.models.Prodotto;
import com.preventivi.HWfinale.services.ClienteService;
import com.preventivi.HWfinale.services.PreventivoService;
import com.preventivi.HWfinale.services.ProdottoService;

@RestController
@RequestMapping("/preventivo")
@CrossOrigin(origins="*", allowedHeaders = "*")
public class PreventivoController {

	@Autowired
	private PreventivoService service;
	@Autowired
	private ClienteService serviceCli;
	@Autowired
	private ProdottoService serviceProd;
	@PostMapping("/insert")
	public Preventivo insert(@RequestBody Preventivo varPreventivo) {
		return service.insert(varPreventivo);
	}
	
	@PutMapping("/insertpreventivo/{preventivo_id}/{cliente_id}")
	public boolean insertPreventivo(@PathVariable int preventivo_id,@PathVariable int cliente_id) {
		Preventivo temp = service.findById(preventivo_id);
		temp.setCliente(serviceCli.findById(cliente_id));
		return service.update(temp);
	}
	
	@PostMapping("/insertprodotto/{preventivo_id}/{prodotto_id}")
	public boolean insertProdotto(@PathVariable int preventivo_id, @PathVariable int prodotto_id) {
		Preventivo temp = service.findById(preventivo_id);
		List<Preventivo> elencoPrev = new ArrayList<Preventivo>();
		Prodotto prod = serviceProd.findById(prodotto_id);
		List<Prodotto> elencoProd = new ArrayList<Prodotto>();
		elencoPrev.add(temp);
		elencoProd.add(prod);
		temp.getProdotti().addAll(elencoProd);
		
		return service.update(temp);
		
		}

	@GetMapping("/{varId}")
	public Preventivo preventivoById(@PathVariable int varId) {
		return service.findById(varId);
	}
	
	@GetMapping("/list")
	public List<Preventivo> allPreventivo() {
		return service.findAll();
	}
	
	@DeleteMapping("/{preventivo_id}")
	public boolean deletePreventivo(@PathVariable int preventivo_id) {
		return service.delete(preventivo_id);
	}
	
	@PutMapping("/update")
	public boolean updatePreventivo(@RequestBody Preventivo varPreventivo) {
		return service.update(varPreventivo);
	}
}
