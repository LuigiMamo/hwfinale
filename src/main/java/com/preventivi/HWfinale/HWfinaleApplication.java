package com.preventivi.HWfinale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HWfinaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(HWfinaleApplication.class, args);
	}

}
