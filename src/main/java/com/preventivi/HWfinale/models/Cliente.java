package com.preventivi.HWfinale.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="cliente")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int cliente_id;
	@Column
	private String nome_azienda;
	@Column
	private String indirizzo;
	
	@OneToMany(mappedBy = "cliente")
	@JsonBackReference
	private List<Preventivo> elencopreventivi;
	
	public Cliente() {
		
	}

	public int getCliente_id() {
		return cliente_id;
	}

	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}

	public String getNome_azienda() {
		return nome_azienda;
	}

	public void setNome_azienda(String nome_azienda) {
		this.nome_azienda = nome_azienda;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public List<Preventivo> getElencopreventivi() {
		return elencopreventivi;
	}

	public void setElencopreventivi(List<Preventivo> elencopreventivi) {
		this.elencopreventivi = elencopreventivi;
	}

	

	
	
	
	

}
