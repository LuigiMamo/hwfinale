package com.preventivi.HWfinale.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name="prodotto")
public class Prodotto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prodotto_id")
	private int prodotto_id;
	@Column
	private String codice;
	@Column
	private String nome;
	@Column
	private int prezzo;
	@ManyToMany
	@JoinTable(name="preventivo_prodotto",
				joinColumns = {@JoinColumn(name="prodotto_rif", referencedColumnName = "prodotto_id")},
				inverseJoinColumns = { @JoinColumn(name="preventivo_rif", referencedColumnName = "preventivo_id")} )
	private List<Preventivo> preventivo;
	
	public Prodotto() {
		
	}

	public int getProdotto_id() {
		return prodotto_id;
	}

	public void setProdotto_id(int prodotto_id) {
		this.prodotto_id = prodotto_id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}

	public List<Preventivo> getPreventivo() {
		return preventivo;
	}

	public void setPreventivo(List<Preventivo> preventivo) {
		this.preventivo = preventivo;
	}

	
	
	
	
}
