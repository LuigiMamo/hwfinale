package com.preventivi.HWfinale.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "preventivo_id")
@Entity
@Table(name="preventivo")
public class Preventivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int preventivo_id;

	@Column
	private String note;
	@Column
	private String emittente;
	@Column
	private String indirizzo_emittente;
	@Column
	private int quantita;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="preventivo_prodotto",
				joinColumns = {@JoinColumn(name="preventivo_rif", referencedColumnName = "preventivo_id")},
				inverseJoinColumns = { @JoinColumn(name="prodotto_rif", referencedColumnName = "prodotto_id")} )
	private List<Prodotto> prodotti;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="cliente_rif")
	@JsonManagedReference
	@JsonIgnore
	private Cliente cliente;
	
	public Preventivo() {
		
	}

	public int getPreventivo_id() {
		return preventivo_id;
	}

	public void setPreventivo_id(int preventivo_id) {
		this.preventivo_id = preventivo_id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getEmittente() {
		return emittente;
	}

	public void setEmittente(String emittente) {
		this.emittente = emittente;
	}

	public String getIndirizzo_emittente() {
		return indirizzo_emittente;
	}

	public void setIndirizzo_emittente(String indirizzo_emittente) {
		this.indirizzo_emittente = indirizzo_emittente;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}

	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
	
	
	
	
}

